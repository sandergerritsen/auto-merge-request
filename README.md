# Gitlab Auto Merge request
Create a merge request on branch creation

## Set up
- Copy `.env.dist` to `.env`
  - Replace all variables in brackets `[ ]` to match your personal credentials
- `docker-compose run composer`
 
## Usage
- `docker-compose run dev`

## Development
- `docker-compose build dev`
- Get help:
  - `docker-compose run dev list`
  - `docker-compose run dev help gitlab-auto:merge-request`
### Update production image
- `docker-compose build prod`
- `docker-compose push prod`

### Composer dependencies
- Create composer environment: `docker-compose build composer`
- Run composer install command: `docker-compose run composer`
- Adding a dependency: `docker-compose run composer composer require vendor/project"`
  - Replace `vendor/project` with the project you like to add
