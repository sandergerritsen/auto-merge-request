<?php declare(strict_types=1);

namespace App\Command;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateMergeRequestCommand extends Command
{
    protected static $defaultName = 'gitlab-auto:merge-request';

    protected function configure(): void
    {
        $this
            ->addArgument('userId', InputArgument::REQUIRED, 'Assignee user ID')
            ->addArgument('projectId', InputArgument::REQUIRED, 'Project ID')
            ->addArgument('sourceRef', InputArgument::REQUIRED, 'Source reference')
            ->addArgument('targetRef', InputArgument::REQUIRED, 'Target reference')
            ->addArgument('userName', InputArgument::REQUIRED, 'Commit user name')
            ->addArgument('title', InputArgument::OPTIONAL, 'Merge request title (optional, defaults to sourceRef');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client  = new Client([
            'base_uri' => getenv('CI_SERVER_URL'), // Base URI is used with relative requests
            'timeout'  => 2.0, // You can set any number of default request options.
        ]);

        try {
            $response = json_decode((string)$client->request(
                'GET',
                '/api/v4/projects/' . $input->getArgument('projectId') . '/merge_requests',
                [
                    'headers' => ['PRIVATE-TOKEN' => getenv('GITLAB_ACCESS_TOKEN')],
                    'query' => [
                        'search' => $input->getArgument('sourceRef'),
                        'state'  => 'opened'
                    ]
                ]
            )->getBody(), true);
        } catch (GuzzleException $e) {
            $output->writeln('Error: ' . $e->getMessage());
            return Command::FAILURE;
        }

        if (count($response) > 0) {
            $output->writeln('Merge request found at ' . ($response[0]['web_url'] ?? 'unknown'));
            return Command::SUCCESS;
        }

        try {
            $response = json_decode((string)$client->request(
                'POST',
                '/api/v4/projects/' . $input->getArgument('projectId') . '/merge_requests',
                [
                    'headers' => ['PRIVATE-TOKEN' => getenv('GITLAB_ACCESS_TOKEN')],
                    'form_params' => [
                        'source_branch' => $input->getArgument('sourceRef'),
                        'target_branch' => $input->getArgument('targetRef'),
                        'title'         => $input->getArgument('title') ?? $input->getArgument('sourceRef'),
                        'assignee_id'   => $input->getArgument('userId'),
                        'description'   => 'Merge request automatically created at commit by ' . $input->getArgument('userName')
                    ]
                ]
            )->getBody(), true);
        } catch (GuzzleException $e) {
            $output->writeln('Error: ' . $e->getMessage());
            return Command::FAILURE;
        }

        $output->writeln('Merge request created at ' . ($response['web_url'] ?? 'unknown'));

        return Command::SUCCESS;
    }
}
