<?php declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use App\Command\CreateMergeRequestCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new CreateMergeRequestCommand());

/** @noinspection PhpUnhandledExceptionInspection */
$application->run();