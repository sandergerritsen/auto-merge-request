FROM php:8.0-cli-alpine3.14 AS app
WORKDIR /app

FROM app AS dev
ENTRYPOINT ["php", "/app/app/gitlab-auto.php"]

FROM app AS composer
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN apk --update add wget \
		     curl \
		     git \
  && rm /var/cache/apk/*


FROM composer AS composerProd
COPY composer.json /app/composer.json
COPY composer.lock /app/composer.lock
RUN composer i



FROM app AS prod
COPY . /app
COPY --from=composerProd /app/vendor /app/vendor
